![CovidInfo APP](http://www.madarme.co/portada-web.png)
# CovidInfo App
CovidInfo app es una aplicación Diseñada para mostrar estadísticas del crecimiento del Covid 19 en nuestro país.
<br>CovidInfo app obtiene los datos de un Archivo JSON que ofrece un banco de datos gratuito donde se encuentra información acerca de los departamentos, ciudades, etc. Se procesa toda esta información para luego ser mostrada en tablas y gráficos para estar informados del crecimiento de la pandemia.

# Tabla de contenido
1. [Características](#características)
1. [Contenido del proyecto](#contenido-del-proyecto)
1. [Tecnologías](#tecnologías)
1. [IDE](#ide)
1. [Instalación](#instalación)
1. [Demo](#demo)
1. [Autores](#autores)
1. [Institucion](#institucion)

# Características
* Implementación y generar gráficos y tablas del api de Google Charts.[Visitar Web](https://developers.google.com/chart)
* Lectura y consumo de datos de un archivo .JSON con tamaño considerable. [ver .JSON](https://raw.githubusercontent.com/ManuelCoronel/covid/main/covid-19.json).
* Implementación y manejo del framework Bootstrap.

# Contenido del proyecto

```
├── assets/
│   ├── css
│   ├── img
│   ├── js
│   ├── vendor
│   
├── forms
│
│── html/
│   ├── rf1.html
│   ├── rf2.html
│   ├── rf3.html
│── js/
│   ├── main.js
├── index.html
└── README.md

```
### CSS
- [estilos.css](https://gitlab.com/andrademanuelalejandroc/covid/-/blob/master/covidInfo/assets/css/style.css): Archivo CSS con estilos general de la web.

### HTML

- [rf1.html](https://gitlab.com/andrademanuelalejandroc/covid/-/blob/master/covidInfo/html/rf1.html): Archivo HTML que realiza la carga y visualizacion de busqueda por genero.
- [rf2.html](https://gitlab.com/andrademanuelalejandroc/covid/-/blob/master/covidInfo/html/rf2.html): Archivo HTML que realiza la carga y visualizacion de busqueda por fuente de contagio.
- [rf3.html](https://gitlab.com/andrademanuelalejandroc/covid/-/blob/master/covidInfo/html/rf3.html): Archivo HTML que realiza la carga y visualizacion de un informe general.

### assets

- Se alojan todos los archivos complementarios de la aplicación como logos, imágenes, estilos adicionales, etc.

### JS

- [main.js](https://gitlab.com/andrademanuelalejandroc/covid/-/blob/master/covidInfo/js/main.js): Archivo de JavaScript que le da la funcionalidad a la aplicación web como la lectura del archivo .JSON y la creación de gráficos con Google Charts. 

### Index
- [index.html](https://gitlab.com/andrademanuelalejandroc/covid/-/blob/master/covidInfo/index.html): Archivo HTML que contiene la pagina principal de la aplicacion.

# Tecnologías
* 🛠  HTML 5
* 🛠  CSS 3
* 🛠  JavaScript
* 🛠  [Boostrap 4](https://getbootstrap.com/) 

# IDE
El proyecto se desarrolla usando:
* 💻 [Suiblime Text](https://www.sublimetext.com/)
* 💻 [Atom](https://atom.io/)
* 💻 [Visual Studio Code](https://code.visualstudio.com/download)

# Instalación
##### **Clone the repository**
``` bash
$ git clone https://gitlab.com/andrademanuelalejandroc/covid.git
```
##### **Go into app's directory**
```bash
$ cd covid
```
##### **Execute**
```bash
$ index.html 
```
# Demo
![Demo](https://gitlab.com/andrademanuelalejandroc/covid/-/raw/master/covidInfo/assets/img/demo.gif)
# Autores 
* ⌨️ **Manuel Alejandro Coronel Anadrade** 
<br> Correo: andrademanuelalejandroc@ufps.edu.co
<br> Gitlab: [andrademanuelalejandroc](https://gitlab.com/andrademanuelalejandroc)
* ⌨️ **Ronald Eduardo Benitez Mejia** 
<br>correo: ronaldeduardobm@ufps.edu.co
<br> Gitlab: [ronaldeduardobm](https://gitlab.com/ronaldeduardobm)
# Institucion
[Universidad Fancisco de Paula Santander](https://ww2.ufps.edu.co/) - [Cúcuta / Colombia](https://www.google.com/maps/place/C%C3%BAcuta,+Norte+de+Santander/@7.9087586,-72.53944,13z/data=!3m1!4b1!4m5!3m4!1s0x8e66459c645dd28b:0x26736c1ff4db5caa!8m2!3d7.8890971!4d-72.4966896)
