var departamentos = new Array();
var url = "https://raw.githubusercontent.com/ManuelCoronel/covid/main/covid-19.json";

async function leerJSON(url) {
  try {
    let response = await fetch(url);
    let user = await response.json();
    return user;
  } catch (err) {
    alert(err);
  }
}
var json = leerJSON(url);

function leerDepartamentos() {

  json.then((datos) => {
    mostrarOpcionesDepartamento(datos);

  });
}



function mostrarOpcionesDepartamento(datos) {

  var msg = "<option value=-1>Seleccione departamento</option>";
  var departamentos = encontrarDepartamentos(datos);

  for (var i = 0; i < departamentos.length; i++) {
    msg +=  /*html*/ ` <option value=${i}>${departamentos[i]}</option>`
  }


  console.log(msg);
  document.getElementById("departamentos").innerHTML = msg;
}

function crearGraficaMunicipios() {



}


function crearTablaGeneral() {

  json.then((datos) => {
    var dataGeneral = new google.visualization.DataTable();
    crearEncabezadoGeneral(dataGeneral);
    crearFilasGeneral(dataGeneral, datos);
    var table = new google.visualization.Table(document.getElementById('tablaGeneral'));
    table.draw(dataGeneral, { showRowNumber: true, width: '100%', height: '100%', pageSize: 12 });


    crearGraficaGeneral(datos);



  });

}
function crearFilasGraficaGeneral(datos, data3) {
  departamentos = encontrarDepartamentos(datos);
  console.log(departamentos);
  data3.addRows(departamentos.length);
  for (var k = 0; k < departamentos.length; k++) {
    data3.setCell(k, 0, departamentos[k]);
    data3.setCell(k, 1, obtenerCasosPositivosPorDepartamento(datos, departamentos[k])[0] + "");
  }
}


function crearEncabezadoGraficaGeneral(data3) {
  data3.addColumn("string", "Departamentos");
  data3.addColumn("number", "casos");

}


function crearGraficaGeneral(datos) {



  var data3 = new google.visualization.DataTable();
  crearEncabezadoGraficaGeneral(data3);
  crearFilasGraficaGeneral(datos, data3);

  var options = {
    axes: {
      x: {
        0: { side: 'top', label: 'Casos por departamentos' } // Top x-axis.
      }
    },

    title: 'Casos por departamentos',
    width: '80%',
    height: 600,
    legend: { position: 'none' },

    bars: 'horizontal', // Required for Material Bar Charts.

  };



  var chart = new google.charts.Bar(document.getElementById('graficaTotal'));
  chart.draw(data3, options);
};








function crearEncabezadoGeneral(dataGeneral) {

  dataGeneral.addColumn('string', 'Departamento');
  dataGeneral.addColumn('string', 'Casos positivos');
  dataGeneral.addColumn('string', 'Hombres');
  dataGeneral.addColumn('string', 'Mujeres');
  dataGeneral.addColumn('string', 'Edad promedio');
  dataGeneral.addColumn('string', 'Importados');
  dataGeneral.addColumn('string', 'Relacionados');
  dataGeneral.addColumn('string', 'Leve');
  dataGeneral.addColumn('string', 'Fallecidos');

}

function crearFilasGeneral(dataGeneral, datos) {

  var departamentos = encontrarDepartamentos(datos);

  dataGeneral.addRows(departamentos.length);

  for (var i = 0; i < departamentos.length; i++) {
    console.log(departamentos[i]);
    var casos = obtenerCasosPositivosPorDepartamento(datos, departamentos[i]);
    dataGeneral.setCell(i, 0, departamentos[i]);
    dataGeneral.setCell(i, 1, casos[0] + "");
    dataGeneral.setCell(i, 2, casos[1] + "");
    dataGeneral.setCell(i, 3, casos[2] + "");
    var edadPromedio = parseInt(casos[3] / casos[0]);
    dataGeneral.setCell(i, 4, (edadPromedio) + "");
    dataGeneral.setCell(i, 5, casos[4] + "")
    dataGeneral.setCell(i, 6, casos[5] + "");
    dataGeneral.setCell(i, 7, casos[6] + "");
    dataGeneral.setCell(i, 8, casos[7] + "");

  }
}



function obtenerCasosPositivosPorDepartamento(datos, departamentos) {
  var casos = 0;
  var mujeres = 0;
  var hombres = 0;
  var edad = 0.0;
  var importados = 0;
  var relacionados = 0;
  var leve = 0;
  var fallecidos = 0;

  var rta = new Array(8);
  for (var i = 0; i < datos.length; i++) {
    if (datos[i].departamento_nom == departamentos) {
      if (datos[i].sexo == "F") {
        mujeres++;
      } else {
        hombres++;
      }

      if (datos[i].fuente_tipo_contagio == "Importado") {
        importados++;
      }

      if (datos[i].fuente_tipo_contagio == "Relacionado") {
        relacionados++;
      }
      if (datos[i].estado == "Leve") {
        leve++;
      }

      if (datos[i].estado == "Fallecido") {
        fallecidos++;
      }

      casos++;
      console.log(edad);
      edad = parseInt(datos[i].edad) + edad;
    }
  }
  rta[0] = casos;
  rta[1] = hombres;
  rta[2] = mujeres;
  rta[3] = edad;
  rta[4] = importados;
  rta[5] = relacionados;
  rta[6] = leve;
  rta[7] = fallecidos;

  return rta;

}



function crearTablaGenero() {

  json.then((datos) => {
    var data = new google.visualization.DataTable();
    crearEncabezadoGenero(data);
    departamento = departamentos[document.getElementById("departamentos").value];
    contagiados = obtenerContagiadosPorDepartamento(datos, departamento);

    var hombresContagiados = calcularHombreContagiados(contagiados);
    var mujeresContagiadas = contagiados.length - hombresContagiados;
    crearFilas(data, datos, departamento);

    crearGrafica(hombresContagiados, mujeresContagiadas);

    var table = new google.visualization.Table(document.getElementById('tabla'));
    table.draw(data, { showRowNumber: true, width: '100%', height: '100%', pageSize: 12 });



  });

}






function crearGrafica(hombresContagiados, mujeresContagiadas) {

  var data2 = new google.visualization.DataTable();
  crearEncabezadoGrafica(data2);
  crearFilasGrafica(data2, hombresContagiados, mujeresContagiadas);


  var options = {
    title: 'Contagios por genero',
    is3D: true,
  };

  var chart = new google.visualization.PieChart(document.getElementById('grafica'));
  chart.draw(data2, options);

}

function crearEncabezadoGrafica(data2) {

  data2.addColumn('string', 'Descripcion');
  data2.addColumn('number', 'valor');

}


function crearFilasGrafica(data2, hombresContagiados, mujeresContagiadas) {
  data2.addRows(2);
  console.log(hombresContagiados);
  data2.setCell(0, 0, "Hombres");
  data2.setCell(0, 1, hombresContagiados);
  data2.setCell(1, 0, "Mujeres");
  data2.setCell(1, 1, mujeresContagiadas);


}

function calcularHombreContagiados(contagiados) {
  console.log(contagiados);
  var hombresContagiados = 0;
  for (var i = 0; i < contagiados.length; i++) {
    console.log(contagiados.sexo);
    if (contagiados[i].sexo == "M") {
      hombresContagiados++;
    }
  }

  return hombresContagiados;

}

function crearFilas(data,datos, departamento) {
  var contagiados = obtenerContagiadosPorDepartamento(datos, departamento);
  data.addRows(contagiados.length);
  for (var s = 0; s < contagiados.length; s++) {


    data.setCell(s, 0, contagiados[s].sexo);
    data.setCell(s, 1, contagiados[s].estado);
    data.setCell(s, 2, contagiados[s].edad);


  }



}


function obtenerContagiadosPorDepartamento(datos, departamento) {
  var vecContagiados = new Array();
  for (var i = 0; i < datos.length; i++) {

    if (datos[i].departamento_nom == departamento) {
      vecContagiados.push(datos[i]);
    }
  }

  return vecContagiados;
}

function crearEncabezadoGenero(data) {


  data.addColumn('string', 'Sexo');
  data.addColumn('string', 'Estado');
  data.addColumn('string', 'Edad');

}

function encontrarDepartamentos(datos) {

  var vecDepartamentos = new Array();

  for (var i = 0; i < datos.length; i++) {
    if (!vecDepartamentos.includes(datos[i].departamento_nom)) {
      vecDepartamentos.push(datos[i].departamento_nom);
    }
  }
  departamentos = vecDepartamentos;
  return vecDepartamentos;
}






function leerMunicipios() {
  json.then((datos) => {
    var municipios = encontrarMunicipios(datos);
    console.log(municipios);
    mostrarMunicipios(municipios);
  });
}

function encontrarMunicipios(datos) {
  let codMunicipio = new Array();
  let nombreMunicipio = new Array();
  let municipios = new Array(2);
  for (let i = 0; i < datos.length; i++) {
    if (!codMunicipio.includes(datos[i].ciudad_municipio)) {
      codMunicipio.push(datos[i].ciudad_municipio);
      nombreMunicipio.push(datos[i].ciudad_municipio_nom)
    }
  }
  municipios[0] = codMunicipio;
  municipios[1] = nombreMunicipio;
  return municipios;
}

function mostrarMunicipios(municipios) {
  console.log(municipios[1].length);

  var msj =/* HTML*/`
            <option value="-1">Ninguno</option>
      `;
  for (let i = 0; i < municipios[0].length; i++) {
    msj += `<option value=${municipios[0][i]}>${municipios[1][i]}</option>`;
    /* console.log(municipios[1][i]); */
  }


  document.getElementById("prueba").innerHTML = msj;
}

function generarEstadisticas() {
  json.then((datos) => {
    let valor = document.getElementById("prueba").value;
    generarTablaMunicipio(valor, datos);
    generalGraficaMunicipio(valor, datos);

  });
}

function generalGraficaMunicipio(value, datos) {
  var pieChart = new google.visualization.DataTable();
  encabezadoPie(pieChart);
  let importados = 0;
  let relacionados = 0;
  let p = 0;
  pieChart.addRows(2);
  for (let i = 0; i < datos.length; i++) {
    if (datos[i].ciudad_municipio == value) {
      if (datos[i].fuente_tipo_contagio == "Importado") {
        importados++;
      }
      if (datos[i].fuente_tipo_contagio == "Relacionado") {
        relacionados++;
      }
    }

  }
  pieChart.setCell(0, 0, "Importado");
  pieChart.setCell(0, 1, importados + "");
  pieChart.setCell(1, 0, "Relacionado");
  pieChart.setCell(1, 1, relacionados + "");
  var options = {
    title: 'Tipo de contagio',
    legend: 'none',
    pieSliceText: 'label',
    slices: {
      0: { offset: 0.1 },
    }
  };
  var chart = new google.visualization.PieChart(document.getElementById('pie'));
  chart.draw(pieChart, options);
}



function encabezadoPie(pieChart) {
  pieChart.addColumn('string', 'descipcion');
  pieChart.addColumn('number', 'valor');
}

function generarTablaMunicipio(value, datos) {
  var dataTable = new google.visualization.DataTable();
  encabezadosTabla(dataTable);
  let p = 0;
  for (let i = 0; i < datos.length; i++) {
    if (datos[i].ciudad_municipio == value) {
      if (datos[i].fuente_tipo_contagio == "Importado") {
        dataTable.addRows(1);
        agregarRow(dataTable, p, datos[i].ciudad_municipio_nom, datos[i].fuente_tipo_contagio, datos[i].estado);
        p++;
      }
      if (datos[i].fuente_tipo_contagio == "Relacionado") {
        dataTable.addRows(1);
        agregarRow(dataTable, p, datos[i].ciudad_municipio_nom, datos[i].fuente_tipo_contagio, datos[i].estado);
        p++;
      }
    }
  }
  var table = new google.visualization.Table(document.getElementById('muni'));
  table.draw(dataTable, { showRowNumber: true, width: '100%', height: '100%', pageSize: 12 });
}

function agregarRow(dataTable, p, municipio, importado, estado) {
  dataTable.setCell(p, 0, municipio + "");
  dataTable.setCell(p, 1, importado + "");
  dataTable.setCell(p, 2, estado + "");
}
function encabezadosTabla(dataTable) {
  dataTable.addColumn('string', 'Municipio');
  dataTable.addColumn('string', 'Fuente de contagio');
  dataTable.addColumn('string', 'Estado');
}

function cuerpoTabla(dataTable) {
  dataTable.addRows([
    ['Mike', { v: 10000, f: '$10,000' }, true],
    ['Jim', { v: 8000, f: '$8,000' }, false],
    ['Alice', { v: 12500, f: '$12,500' }, true],
    ['Bob', { v: 7000, f: '$7,000' }, true]
  ]);
}




